FROM clojure:alpine
COPY . /usr/src/app
WORKDIR /usr/src/app
CMD ["lein", "run", ":london-tripping/app", ":london-tripping/runtime"]