(defproject london-tripping "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.9.0-alpha16"]
                 [org.arachne-framework/arachne-core "0.2.0-master-0099-dbcc8df"]
                 [datascript "0.15.5"]
                 [ch.qos.logback/logback-classic "1.1.3"]]
  :source-paths ["src" "config"]
  :repositories [["arachne-dev" "http://maven.arachne-framework.org/artifactory/arachne-dev"]]
  :main arachne.run)
