(ns ^:config london-tripping.config
  (:require [arachne.core.dsl :as a]))

(a/id :london-tripping/widget-1 (a/component 'london-tripping.core/make-widget))

(a/id :london-tripping/runtime (a/runtime [:london-tripping/widget-1]))
